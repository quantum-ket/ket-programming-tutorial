from ket import *
from ket import code_ket


def entangle(a: quant, b: quant):
    return cnot(H(a), b)


def teleport(quantum_message: quant, entangled_qubit: quant):
    adj(entangle, quantum_message, entangled_qubit)
    return measure(entangled_qubit), measure(quantum_message)


@code_ket
def decode(classical_message: tuple[int, int], qubit: quant):
    if classical_message[0] == 1:
        X(qubit)

    if classical_message[1] == 1:
        Z(qubit)


if __name__ == '__main__':
    from math import pi

    alice_message = phase(pi/4, H(quant()))

    alice_message_dump = dump(alice_message)

    alice_qubit, bob_qubit = entangle(*quant(2))

    classical_message = teleport(
        quantum_message=alice_message,
        entangled_qubit=alice_qubit
    )

    decode(classical_message, bob_qubit)

    bob_qubit_dump = dump(bob_qubit)

    print('Alice Message:')
    print(alice_message_dump.show())

    print('Bob Qubit:')
    print(bob_qubit_dump.show())
